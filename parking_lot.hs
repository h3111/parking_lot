{-|
  parking_lot.hs - calculate number of cars and bikes
                   from number of vehicles and wheels

  Usage:    parking_lot NUM-VEHICLES NUM-WHEELS
  Example:  parking_lot 3 10

  Compile:  ghc --make -Wall parking_lot
  Lint:     hlint -s parking_lot.hs

  Author:   Wolfgang <haskell@wu6ch.de>

  Inspired by:

  minikurs 2: Die Programmieraufgabe (German)
  https://namespace-cpp.de/std/doku.php/lernen/minikurs/parkplatz
-}


{-# LANGUAGE InstanceSigs #-}


import Control.Monad ((>=>))

import Data.Bool (bool)
import Data.Maybe (maybe)

import System.Environment (getArgs)

import Text.Read (readMaybe)


{-|
  'Counts' is a record type that holds the counted numbers of vehicles and
  wheels.
-}
data Counts a =
  Counts
    { cntVehicles :: a
    , cntWheels   :: a
    }


{-|
  "Special" instance of 'show' for 'Counts' record type.

  >>> Counts 1 4
  1 vehicle, 4 wheels
-}
instance (Integral a, Show a) => Show (Counts a) where
  show :: Counts a -> String
  show counts = mconcat [show' v "vehicle", ", ", show' w "wheel"]
    where
      v = cntVehicles counts
      w = cntWheels   counts


{-|
  'Crafts' is a record type that holds the calculated numbers of cars and bikes.
-}
data Crafts a =
  Crafts
    { crCars  :: a
    , crBikes :: a
    }


{-|
  "Special" instance of 'show' for 'Crafts' record type.

  >>> Crafts 3 1
  3 cars, 1 bike
-}
instance (Integral a, Show a) => Show (Crafts a) where
  show :: Crafts a -> String
  show crafts = mconcat [show' c "car", ", ", show' b "bike"]
    where
      c = crCars  crafts
      b = crBikes crafts


{-|
  Takes a numerical value and a string and returns a concatenated string that
  contains the string representation of the numerical value and the string
  (with or without appended 's').

  >>> show' 1 "beer"
  "1 beer"

  >>> show' 2 "beer"
  "2 beers"
-}
show' :: (Integral a, Show a) => a -> String -> String
show' n s = mconcat [show n, " ", s, bool "s" "" (n == 1)]


{-|
  Takes a Count value and a string and returns the string representation of the
  Count value and the string with ' -> 'in between.

  >>> showMsg (Counts 1 4) $ show $ Crafts 1 0
  "1 vehicle, 4 wheels -> 1 car, 0 bikes"

  >>> showMsg (Counts 1 5) "What?! One vehicle with five wheels?"
  "1 vehicle, 5 wheels -> What?! One vehicle with five wheels?"
-}
showMsg :: (Integral a, Show a) => Counts a -> String -> String
showMsg counts msg = mconcat [show counts, " -> ", msg]


{-|
  Takes a list of strings (the command line arguments) and returns an Either
  Counts value, if the string list contains (at least) 2 numerical values.

  >>> argsToCounts ["42", "43"]
  Right 42 vehicles, 43 wheels

  >>> argsToCounts ["42", "43", "44"]
  Right 42 vehicles, 43 wheels

  >>> argsToCounts []
  Left "Two numerical values needed!"

  >>> argsToCounts ["42"]
  Left "Two numerical values needed!"

  >>> argsToCounts ["42", "a"]
  Left "Two numerical values needed!"

  >>> argsToCounts ["42", "43", "a"]
  Left "Two numerical values needed!"
-}
argsToCounts :: (Integral a, Read a) => [String] -> Either String (Counts a)
argsToCounts args = maybe (Left err) toCounts (mapM readMaybe args)
  where
    toCounts :: [a] -> Either String (Counts a)
    toCounts []      = Left err
    toCounts [_]     = Left err
    toCounts (v:w:_) = Right $ Counts v w
    err = "Two numerical values needed!"


{-|
  Takes a Count value, checks it for plausibility, and returns it as an Either
  Count value in case of okay. Otherwise, the function returns an Either String
  value as an error message.

  >>> checkCounts (Counts 1 4)
  Right 1 vehicle, 4 wheels

  >>> checkCounts (Counts 1 5)
  Left "1 vehicle, 5 wheels -> Number of wheels is odd!"
-}
checkCounts :: (Integral a, Show a) => Counts a -> Either String (Counts a)
checkCounts counts
  | v < 0     = Left $ showMsg counts "Number of vehicles is negative!"
  | w < 0     = Left $ showMsg counts "Number of wheels is negative!"
  | odd w     = Left $ showMsg counts "Number of wheels is odd!"
  | w < 2 * v = Left $ showMsg counts "Too few wheels for this number of vehicles!"
  | w > 4 * v = Left $ showMsg counts "Too many wheels for this number of vehicles!"
  | otherwise = Right counts
  where
    v = cntVehicles counts
    w = cntWheels   counts


{-|
  Takes a Count value and returns a Craft value that contains the calculated number
  of cars and bikes.

  >>> calcCrafts (Counts 3 10)
  2 cars, 1 bike
-}
calcCrafts :: (Integral a) => Counts a -> Crafts a
calcCrafts (Counts v w) = Crafts (w `div` 2 - v) (2 * v - w `div` 2)


{-|
  Takes a list of strings (the command line arguments), converts them to
  numerical values, treats these values as number of vehicles and number of
  wheels, calculates the number of cars and the number of bikes, and returns the
  result (or an error message) as String.

  >>> calcCraftsFromArgs ["3", "10"]
  "3 vehicles, 10 wheels -> 2 cars, 1 bike"

  >>> calcCraftsFromArgs ["3", "4"]
  "3 vehicles, 4 wheels -> Too few wheels for this number of vehicles!"

  Watch that the type signature of the function calcCrafts' defines the Integral
  type a as an Int. As a consequence, an overflow might occur for really big
  numbers of vehicles or wheels.

  If you cannot live with this, modify the function type signature as follows:
  > calcCrafts' :: Counts Integer -> Crafts Integer
-}
calcCraftsFromArgs :: [String] -> String
calcCraftsFromArgs args =
  case (argsToCounts >=> checkCounts) args of
    Left err     -> err
    Right counts -> showMsg counts $ show $ calcCrafts' counts
      where
        calcCrafts' :: Counts Int -> Crafts Int
        calcCrafts' = calcCrafts


{-|
  'main': Evaluates the command line arguments and calculates the numbers of
  cars and bikes from the given numbers of vehicles and bikes. For '-h', it
  outputs a short usage.
-}
main :: IO ()
main = do
  args <- getArgs
  bool (putStrLn $ calcCraftsFromArgs args) (putStrLn usage) ("-h" `elem` args)
  where
    usage =
      unlines
        [ "Usage:   parking_lot NUM-VEHICLES NUM-WHEELS"
        , "Example: parking_lot 3 10"
        ]


-- EOF
