# Parking lot


## The challenge

[_minikurs 2: Die Programmieraufgabe_ (German)](https://namespace-cpp.de/std/doku.php/lernen/minikurs/parkplatz)

_Cars and bikes (without sidecars) are parked on a parking lot. The overall
number of vehicles is **v**, and the overall number of wheels is **w**.
Calculate the number of cars **c** and the number of bikes **b**_.


## How to calculate

Input (_"counts"_):

````math
\begin{align}
  v &= \text{number of vehicles} \\
  w &= \text{number of wheels}
\end{align}
````

Output (_"crafts"_):

````math
\begin{align}
  c &= \text{number of cars} \\
  b &= \text{number of bikes}
\end{align}
````

The number of vehicles is the sum of the number of cars and the number of bikes:

````math
\begin{align}
  v &= c + b \\
  \Leftrightarrow b &= v - c \\
  \Leftrightarrow c &= v - b
\end{align}
````

Each car has 4 wheels; each bike has 2 wheels. Therefore, the total number of
wheels is:

````math
\begin{align}
  w &= 4 \cdot c + 2 \cdot b
\end{align}
````

With (6):

````math
\begin{align}
  w &= 4 \cdot c + 2 \cdot (v - c) \\
  \Leftrightarrow w &= 4 \cdot c + 2 \cdot v - 2 \cdot c \\
  \Leftrightarrow w &= 2 \cdot c + 2 \cdot v \\
  \Leftrightarrow 2 \cdot c &= w - 2 \cdot v \\
  \Leftrightarrow c &= \frac{1}{2} \cdot w - v
\end{align}
````

With (7):

````math
\begin{align}
  w &= 4 \cdot (v - b) + 2 \cdot b \\
  \Leftrightarrow w &= 4 \cdot v - 4 \cdot b + 2 \cdot b \\
  \Leftrightarrow w &= 4 \cdot v - 2 \cdot b \\
  \Leftrightarrow 2 \cdot b &= 4 \cdot v - w \\
  \Leftrightarrow b &= 2 \cdot v - \frac{1}{2} \cdot w
\end{align}
````

Test with (5), (13), (18):

````math
\begin{align}
  c + b = \frac{1}{2} \cdot w - v + 2 \cdot v - \frac{1}{2} \cdot w = v
\end{align}
````

Example with (13) and (18):

````math
\begin{align}
  v = 5, w = 14 \rightarrow c = \frac{1}{2} \cdot 14 - 5 = 2 \text{ and } b = 2 \cdot 5 - \frac{1}{2} \cdot 14 = 3
\end{align}
````


### Constraints:

* Neither the number of vehicles nor the number of wheels may be negative,
  otherwise you get negative values for the number of cars or the number of
  bikes.

* The number of wheels may not be odd, due to the term
  $`\frac{1}{2} \cdot w`$.

* The number of wheels must be 2 times the number of vehicles or greater,
  otherwise you get a negative number of cars.

* The number of wheels must be 4 times the number of vehicles or less,
  otherwise you get a negative number of bikes.


## [parking_lot.hs](parking_lot.hs)


### Compiling

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall parking_lot
````

### Linting

````bash
$ hlint -s parking_lot.hs
````

### "Installation"

Copy `parking_lot` to a directory that is listed in the environment variable
`PATH`, e.g. `~/bin` (or create a symlink).


### Usage

Call `parking_lot` this way to get a short usage message:

````bash
$ parking_lot -h
Usage:   parking_lot NUM-VEHICLES NUM-WHEELS
Example: parking_lot 3 10
````

Calculate the number of cars and the number of bikes for 3 vehicles and
10 wheels counted:

````bash
$ parking_lot 3 10
3 vehicles, 10 wheels -> 2 cars, 1 bike
````

Reject an erroneous count:

````bash
$ parking_lot 1 8
1 vehicle, 8 wheels -> Too many wheels for this number of vehicles!
````

You get an overflow for really big numbers:

````bash
$ parking_lot 9223372036854775808 19223372036854775808
-9223372036854775808 vehicles, 776627963145224192 wheels -> Number of vehicles is negative!
````

See function `calcCraftsFromArgs` in [parking_lot.hs](parking_lot.hs), how to
prevent this overflow by using _Integer_ instead of _Int_.

After that, it works also for a really large parking lot:

````bash
$ parking_lot 9223372036854775808 19223372036854775808
9223372036854775808 vehicles, 19223372036854775808 wheels -> 388313981572612096 cars, 8835058055282163712 bikes
````


<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->
